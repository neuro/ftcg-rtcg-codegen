﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется посредством следующего 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("WindowsFormsApplication2Tests")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyProduct("WindowsFormsApplication2Tests")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Задание значения false для атрибута ComVisible приведет к тому, что типы из этой сборки станут невидимыми 
// для COM-компонентов. Если к одному из типов этой сборки необходимо обращаться из 
// модели COM, задайте для атрибута ComVisible этого типа значение true.
[assembly: ComVisible(false)]

// Если данный проект доступен для модели COM, следующий GUID используется в качестве идентификатора библиотеки типов
[assembly: Guid("dca4149b-890f-4ee4-b961-1efab52495cd")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Редакция
//
// Можно задать все значения или принять номера построения и редакции по умолчанию 
// используя "*", как показано ниже:
// [сборка: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
