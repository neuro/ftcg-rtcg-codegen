﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication2;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace WindowsFormsApplication2.Tests
{
    [TestClass()]
    public class ShaParserTests
    {
        [TestMethod()]
        public void SetVarsTest()
        {
            string test = 
                  "a0 = 1 \r\n"  // 0
                + "b1 =\"c\" \r\n"
                + "d2 = 2 + 2 \r\n " // 2
                + "e3 = \"\\\"\" \r\n"
                + "f4 = 1 + 2 * 3 \r\n" // 4
                + "g5 = (1 + 1) * 3 \r\n"
                + "s6 = toUpper(\"t\") \r\n " // 6
                + "t7 = toLower(\"L\") + toUpper(\"l\") \r\n"
                + "p8 = 1 / 2 \r\n" // 8
                + "o9 = 1 / 2 + 1 / 2 \r\n"
                + "print \"test\" \r\n"
                + "test(\"test\") \r\n";
            ShaParser p = new ShaParser();
            SDK r = p.Parse(test);
            Assert.AreEqual("1", r.Variables[0].Value);
            Assert.AreEqual("a0",r.Variables[0].Name);
            Assert.AreEqual("c", r.Variables[1].Value);
            Assert.AreEqual("4", r.Variables[2].Value);
            Assert.AreEqual("\\\"", r.Variables[3].Value, message: r.Variables[3].Value.Length.ToString());
            Assert.AreEqual("7", r.Variables[4].Value);
            Assert.AreEqual("6", r.Variables[5].Value);
            Assert.AreEqual("T", r.Variables[6].Value);
            Assert.AreEqual("lL", r.Variables[7].Value);
            Assert.AreEqual("0,5", r.Variables[8].Value);
            Assert.AreEqual("1", r.Variables[9].Value);
            Assert.AreEqual("testtest\r\n", SDK.Log);
        }
    }
}
