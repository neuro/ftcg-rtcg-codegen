﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void code_TextChanged(object sender, EventArgs e)
        {
            SDK.Log = "";
            SDK RESULT = (new ShaParser()).Parse(code.Text);

            result.Items.Clear();
            foreach (Variable var in RESULT.Variables)
            {
                result.Items.Add(var.Name + "=" + var.Value);
            }

            Log.Text = SDK.Log;



            string[] Operations = { "+", "-", "*", "/", "%" };
            int quote = -1;
            int eof = 0;
            int SelectionStart = code.SelectionStart;
            int SelectionLength = code.SelectionLength;

            for (int i = 0; i < code.Text.Length; i++)
            {
                if (code.Text[i] == "="[0])
                {
                    code.Select(i, 1);
                    code.SelectionColor = Color.Crimson;

                    code.Select(eof, i - eof);
                    code.SelectionColor = Color.Khaki;
                }
                if (code.Text.Length - 4 > i && code.Text.Substring(i, 5) == "print")
                {
                    code.Select(i, 5);
                    code.SelectionColor = Color.DarkGreen;
                }
                if (code.Text.Length - 3 > i && code.Text.Substring(i, 4) == "func")
                {
                    code.Select(i, 4);
                    code.SelectionColor = Color.DarkGoldenrod;
                    
                }
                if (Operations.Contains(code.Text.Substring(i, 1)))
                {
                    code.Select(i, 1);
                    code.SelectionColor = Color.Yellow;
                }
                if (code.Text[i] == "\""[0])
                {
                    if (quote != -1)
                    {
                        code.Select(quote, i - quote+1);
                        code.SelectionColor = Color.DeepSkyBlue;
                        quote = -1;
                    }
                    else
                    {
                        quote = i;
                    }
                }

                if (code.Text[i] == "\n"[0])
                {
                    eof = i;
                }
            }

            code.Select(SelectionStart, SelectionLength);

        }

        private void Form1_Load(object sender, EventArgs e)
        {// name = input(\"ttt\", name)
            code.Text = "test() \r\n test(\"\") \r\n ccc = 3 + 1 * 2 \r\n dd = \"test \" + 1 + \" \" \r\n ww = 1 + \"test\"\r\n name = \"vasy\" \r\n str = \"Hello \" + \"World!\" \r\n a = 2 + 1 + 1 \r\n b = 2 + a \r\n c = a * b + a \r\n f = 3 \r\n x = f * c \r\n sum = f + c \r\n min = f - c \r\n d = f / c \r\n w = (2+1)*3 \r\n print str+\"\\r\\n\" \r\n namaae = toUpper(\"введите имя \") \r\n print \"Hello \"+name + \"\\r\\n\"\r\n func ftest(a,b=\"default value\",d=1)\r\n   test(\"Выводи из ftest(a = \" + a +\", b =\"+ b +\", d = \"+d+\")\") \r\n return \"return(a=\"+a+\")\" \r\n end \r\n retVar = ftest(\"call ftest\") \r\n test(retVar)\r\n";
        }

        private void input_Enter(object sender, EventArgs e)
        {
            
            
        }

        private void input_KeyUp(object sender, KeyEventArgs e)
        {
            //Language.setVariable("input",);
            
            if (SDK.input != "" && e.KeyData == Keys.Enter)
            {
                /*this.input.Text = "";
                SDK.input = this.input.Text;
                SDK.Log = "";
                //this.code_TextChanged(sender, e);
                SDK RESULT = ShaParser.Instance.Parse(code.Text);

                result.Items.Clear();
                foreach (Variable var in RESULT.Variables)
                {
                    result.Items.Add(var.Name + "=" + var.Value);
                }

                Log.Text = SDK.Log;*/

            }
        }
    }
}
