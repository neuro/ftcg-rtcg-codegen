﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.code = new System.Windows.Forms.RichTextBox();
            this.result = new System.Windows.Forms.ListBox();
            this.Log = new System.Windows.Forms.RichTextBox();
            this.input = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // code
            // 
            this.code.BackColor = System.Drawing.SystemColors.MenuText;
            this.code.BulletIndent = 5;
            this.code.Dock = System.Windows.Forms.DockStyle.Top;
            this.code.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.code.Location = new System.Drawing.Point(0, 0);
            this.code.Margin = new System.Windows.Forms.Padding(0);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(755, 417);
            this.code.TabIndex = 0;
            this.code.Text = "";
            this.code.TextChanged += new System.EventHandler(this.code_TextChanged);
            // 
            // result
            // 
            this.result.FormattingEnabled = true;
            this.result.Location = new System.Drawing.Point(0, 412);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(332, 212);
            this.result.TabIndex = 1;
            // 
            // Log
            // 
            this.Log.Location = new System.Drawing.Point(331, 412);
            this.Log.Name = "Log";
            this.Log.Size = new System.Drawing.Size(424, 187);
            this.Log.TabIndex = 2;
            this.Log.Text = "";
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(331, 600);
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(424, 20);
            this.input.TabIndex = 3;
            this.input.Enter += new System.EventHandler(this.input_Enter);
            this.input.KeyUp += new System.Windows.Forms.KeyEventHandler(this.input_KeyUp);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 622);
            this.Controls.Add(this.input);
            this.Controls.Add(this.Log);
            this.Controls.Add(this.result);
            this.Controls.Add(this.code);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox result;
        private System.Windows.Forms.RichTextBox Log;
        public System.Windows.Forms.RichTextBox code;
        private System.Windows.Forms.TextBox input;
    }
}

