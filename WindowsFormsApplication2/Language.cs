﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormsApplication2
{
    class Language
    {
        public SDK SDK;
        public string getVariable(string name)
        {
            foreach (Variable VAR in this.SDK.Variables)
            {
                if (VAR.Name == name && this.SDK.Namespace == VAR.Namespace)
                    return VAR.Value;
            }

            return "";
        }

        public void setVariable(string name, string value, string Namespace)
        {
            bool isUndefined = true;
            foreach (Variable VAR in this.SDK.Variables)
            {
                if (VAR.Name == name && this.SDK.Namespace == VAR.Namespace)
                {
                    VAR.Value = value;
                    VAR.Namespace = Namespace;
                    isUndefined = true;
                }
            }

            if (isUndefined)
            {
                Variable var = new Variable();
                var.Name = name;
                var.Value = value;
                var.Namespace = Namespace;
                this.SDK.Variables.Add(var);
            }
        }

        public bool isNumber(string number)
        {
            double n;
            return double.TryParse(number, out n);
        }

        public string call(string name, List<string> _params)
        {
            name = name.Trim();
            for (int i = 0; i < _params.Count(); i++ )
            {
                if (isString(_params[i]))
                    _params[i] = _params[i].Substring(1, _params[i].Length - 2);
            }
            switch (name)
            {
                case "toUpper":
                    return _params[0].ToUpper();
                case "toLower":
                    return _params[0].ToLower();
                case "test":
                    if (_params.Count() > 0)
                    {
                        SDK.Log += _params[0] + "\r\n";
                        return _params[0] + "\r\n";//.Substring(1, _params[0].Length - 2)
                    }
                    else
                    {
                        SDK.Log += "~~test~~\r\n";
                        return "~~test~~\r\n";
                    }
                    
                case "input":
                    SDK.Log += _params[0];
                    if (SDK.input != "")
                    {
                        return SDK.input;
                    }
                    else
                    {
                        SDK.input = _params[1];
                        return " ";
                    }
               
            }

            foreach (Function func in this.SDK.Functions)
            {
                if (func.Name == name)
                {
                    ShaParser parser = new ShaParser();
                    int i = 0;
                    foreach (string par in func.Params)
                    {
                        string[] data = par.Split('=');
                        if (i >= _params.Count())
                        {
                            if (data.Length > 1)
                            {
                                data[1] = data[1].Trim(' ');
                                if (isString(data[1]))
                                {
                                    data[1] = data[1].Substring(1, data[1].Length - 2);
                                }
                                parser.setVariable(data[0].Trim(' '), data[1], name);
                            }
                        }
                        else
                        {
                            if (data.Length > 1)
                            {

                                parser.setVariable(data[0].Trim(' '), data[1], name);
                            }
                            else
                            {
                                if (isString(_params[i]))
                                {
                                    parser.setVariable(par, _params[i].Substring(1, _params[i].Length - 2), name);
                                }
                                else
                                {
                                    parser.setVariable(par, _params[i], name);
                                }
                            }
                        }
                        i++;
                    }

                    
                    
                    SDK sdk = parser.Parse(func.Body, func.Name);
                    return sdk._return;
                }
            }

            return "";
        }

        public double Calc(char operation, double result, int IntVar)
        {
                return Calc(operation, result, Convert.ToDouble(IntVar));
        }

        public double Calc(char operation, double result, double IntVar)
        {
            switch (operation)
            {
                case '+':
                    result += IntVar;
                    break;
                case '-':
                    result -= IntVar;
                    break;
                case '*':
                    result *= IntVar;
                    break;
                case '/':
                    result /= IntVar;
                    break;
                case '%':
                    result %= IntVar;
                    break;
            }

            return result;
        }

        public int getIntVal(string VarName)
        {
            int IntVal;
            if (!this.isNumber(VarName))
            {
                IntVal = Convert.ToInt32(this.getVariable(VarName));
            }
            else
            {
                IntVal = Convert.ToInt32(VarName);
            }

            return IntVal;
        }

        public bool isString(string Str)
        {
             return  Str.Length > 1 && Str[0] == '"';
        }

        public string SubStringVar(string value, int start, int len)
        {
            return value.Substring(start, len).Trim(ShaParser.trim).Trim("\""[0]);
        }

        public int SubIntVar(string value, int start, int len)
        {
            string VarName = value.Substring(start, len).Trim(ShaParser.trim);
            return this.getIntVal(VarName);
        }
        public List<string> parseFunctionParams(string _params)
        {
            List<string> ppar = new List<string>();
            if (_params != "")
            {
                _params += ",";
                bool isQuote = false;
                
                int oldDelim = -1;
                for (int i = 0; i < _params.Length; i++)
                {
                    if (_params[i] == '"' && (i == 0 || _params[i - 1] != '\\'))
                    {
                        isQuote = !isQuote;
                    }
                    if (!isQuote)
                    {
                        if (_params[i] == ',')
                        {
                            string val = _params.Substring(oldDelim + 1, i - oldDelim - 1);
                            if (getType(val) == Type.Number)
                            {
                                ppar.Add(val);
                            }
                            else
                            {
                                ppar.Add(parseLine(val));
                            }
                            oldDelim = i + 1;
                        }
                    }
                }
            }

            return ppar;
        }


        public Type getType(string val)
        {
            if (val[0] == '"')
            {
                return Type.String;
            }
            else if (this.isNumber(val))
            {
                return Type.Number;
            }
            else
            {
                if (this.getVariable(val) != "")
                    return Type.Variable;
                else
                    return Type.String;
            }
        }

        public enum Type : byte { Number, String, Variable, Operation };
        public struct Value
        {
            public string value;
            public Type type;
            public int position;
        }

        public string parseLine(string str)
        {
            str += ";";
            char[] operations = { '+', '-', '*', '/', '%', ';', '(', ')'};

            char[] highPriorityOp = { '*', '/', '%', '(', ')'};
            List<Value> values= new List<Value>();
            int oldOpPos = -1;
            bool isQuote = false;
            bool isHighPriority = false;
            bool isBracket = false;
            int bracketPosition = -1;
            int bracketCount = 0;
            char oldOp = '0';
            string thisFunction = "";
            bool isFunction = false;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '"' && (i == 0 || str[i - 1] != '\\'))
                {
                    isQuote = !isQuote;
                }


                if (!isQuote)
                {
                    if (str[i] == ')' || str[i] == '(')
                        isBracket = !isBracket;

                    if (str[i] == '(' && !isFunction)
                    {
                        // is not function
                        if (i == 0 || operations.Contains(str[i - 1]) || str[i - 1] == ' ')
                        {
                            if (bracketCount == 0)
                                bracketPosition = i;
                            bracketCount++;
                        }
                        else
                        {
                            bracketPosition = i;
                            thisFunction = str.Substring(oldOpPos + 1, i - oldOpPos - 1);
                            isFunction = true;
                        }
                    }
                    if (str[i] == ')')
                    {
                        if (isFunction)
                        {
                            string _params = str.Substring(bracketPosition + 1, i - bracketPosition - 1);
                            string newStr = call(thisFunction, parseFunctionParams(_params));
                            Value val = new Value();
                            val.type = getType(newStr);
                            val.value = newStr;
                            val.position = bracketPosition - thisFunction.Length;
                            values.Add(val);
                            isFunction = false;
                        }
                        bracketCount--;
                        oldOp = ')';
                    }

                    if (str[i] == ')' && bracketCount == 0)
                    {
                        string newStr = parseLine(str.Substring(bracketPosition+1,i-bracketPosition-1));
                        Value val = new Value();
                        val.type = getType(newStr);
                        val.value = newStr;
                        val.position = bracketPosition+1;
                        bracketPosition = i;
                        values.Add(val);
                        continue;
                    }
                }

                if (operations.Contains(str[i]) && !isQuote && !isBracket && !isFunction)
                {
                    if (highPriorityOp.Contains(str[i]))
                        isHighPriority = true;
                    if (oldOp != ')')
                    {
                        Value val2 = new Value();
                        val2.position = oldOpPos + 1;
                        val2.value = str.Substring(oldOpPos + 1, i - oldOpPos - 1).Trim(' ');

                        val2.type = getType(val2.value);
                        if (val2.type == Type.String)
                            val2.value = val2.value.Substring(1, val2.value.Length - 2);

                        values.Add(val2);
                    }

                    if (str[i] == ';')
                        break;

                    Value val = new Value();
                    val.type = Type.Operation;
                    val.value = str[i].ToString();
                    val.position = i;
                    values.Add(val);

                    oldOpPos = i;
                    oldOp = str[i];
                }
            }



            if (isHighPriority)
            {
                for (int i = 0; i < values.Count(); i++)
                {
                    if (i > 1 && values[i - 1].type == Type.Operation && highPriorityOp.Contains(values[i - 1].value[0]))
                    {
                        if (values[i - 1].value != "(" && values[i - 1].value != ")")
                        {
                            Value oldVal = values[i - 2];
                            oldVal.type = Type.Number;

                            string val1 = values[i - 2].value;
                            string val2 = values[i].value;

                            if (values[i - 2].type == Type.Variable)
                                val1 = this.getVariable(val1);
                            if (values[i].type == Type.Variable)
                                val2 = this.getVariable(val2);

                            oldVal.value = Convert.ToString(Calc(values[i - 1].value[0], Convert.ToDouble(val1), Convert.ToDouble(val2)));


                            values.RemoveAt(i - 2);
                            values.Insert(i - 2, oldVal);
                            values.RemoveRange(i - 1, 2);
                            i = i - 2;
                        }
                    }

                }
            }

            double nresult = 0;
            string result = "";
            bool strResult = false;
            for (int i = 0; i < values.Count(); i++)
            {
                
                string val = values[i].value;
                Type type = values[i].type;

                if (values[i].type == Type.Variable)
                {
                    val = this.getVariable(val);
                    type = this.getType(val);
                }

                if (type == Type.Number)
                {
                    if (!strResult)
                    {
                        if (i == 0)
                        {
                            nresult = Convert.ToDouble(val);
                        }
                        else
                        {
                            nresult = Calc(values[i - 1].value[0], nresult, Convert.ToDouble(val));
                        }
                    }
                    else
                    {
                        result += val;
                    }
                }
                else if (type == Type.String)
                {
                    result += val;
                    if (i > 1 && !strResult)
                    {
                        result = nresult.ToString() + result;
                    }
                    strResult = true;
                }
                
            }


            if (!strResult)
            {
                result = nresult.ToString();
            }

            result = result.Replace(@"\r", "\r").Replace(@"\n", "\n");
            
            return result;
            
        }
    }
}
