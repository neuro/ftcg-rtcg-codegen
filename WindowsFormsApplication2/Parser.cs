﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsApplication2;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public class Variable
    {
        public string Name;
        public string Value;
        public string Namespace = "";
    }

    public struct Function
    {
        public string Name;
        public List<string> Params;
        public string Body;
    }


    public class SDK
    {
        public List<Variable> Variables = new List<Variable>();
        public List<Function> Functions = new List<Function>();
        public static string Log = "";
        public static string input = "";
        public string Namespace { get; set; }
        public string _return;
    };

    
    public class ShaParser
    {
        private Dictionary<string, int> PosData;
        private string StringScheme;
        private SDK SDK;
        private Language lang;
        public static char[] trim = { ' ', '\t', '\r', '\n' };


        public ShaParser()
        {
            this.SDK = new SDK();
            this.PosData = new Dictionary<string, int>();
            this.lang = new Language();
            this.lang.SDK = this.SDK;
        }

        public void setVariable(string name, string value, string Namespace)
        {
            this.lang.setVariable(name, value, Namespace);
        }

        /*
         Parse *.sha Scheme by FileName
         */
        public SDK ParseFile(string Filename, string Namespace = "")
        {
            if (File.Exists(Filename))
            {
                return this.Parse(File.ReadAllText(Filename), Namespace);
            }
            else
            {
                return SDK;
            }
        }
        /*
          Парсит схему их строки
        */

        public SDK Parse(string SHA, string Namespace = "")
        {
            this.StringScheme = "\n" + SHA + "\n";
            
            this.SDK.Namespace = Namespace;
            bool IsQuote = false;
            int funcStopParamPos = 0;
            int funcStartParamPos = 0;
            int thisVariableNum = 0;

            try {

                

                for (int i = 0; i < this.StringScheme.Length; i++)
                {
                    // Если мы в кавычках
                    if (this.SubIs("\"", i))
                    {
                        IsQuote = !IsQuote;
                    }

                    if (IsQuote)
                    {
                        continue;
                    }

                    // ----------------------
                    this.SetPosIf("print", i);
                    this.SetPosIf("return", i);

                    
                    if (this.SetPosIf("(", i) && this.hasPos("func") && funcStartParamPos == 0)
                    {
                        funcStartParamPos = this.getPos("(");
                    }
                    if (this.SetPosIf(")", i) && this.hasPos("func") && funcStopParamPos == 0)
                    {
                        funcStopParamPos = this.getPos(")");
                    }
                    this.SetPosIf("func", i);

                    if (!this.hasPos("func"))
                    {

                        if (this.SetPosIf("\n", i))
                        {
                            if (this.hasPos("print"))
                            {
                                string value = this.getSub("print", "\n").Trim(ShaParser.trim);
                                SDK.Log += lang.parseLine(value);
                                this.deletePos("print");
                            }

                            //variable value
                            if (this.hasPos("="))
                            {
                                string value = this.getSub("=", "\n").Trim(trim);
                                this.SDK.Variables[thisVariableNum].Value = lang.parseLine(value);
                                this.deletePos("=");
                            }
                        }
                        // variable name
                        if (this.SetPosIf("=", i))
                        {
                            Variable var = new Variable();
                            var.Name = this.getSub("\n", "=").Trim(ShaParser.trim);
                            this.SDK.Variables.Add(var);
                            thisVariableNum = this.SDK.Variables.Count - 1;
                        }

                        if (this.hasPos("(") && this.SetPosIf(")", i))
                        {
                            if (this.hasPos("(") && this.hasPos(")"))
                            {
                                string name = this.getSub("\n", "(");
                                string _params = this.getSub("(", ")");
                                lang.call(name, lang.parseFunctionParams(_params));
                            }
                        }
                    }
                    else
                    {
                        if (this.SetPosIf("end", i) || hasPos("end"))
                        {
                            char[] trim = { '\r', '\n', ' ', '\t' };
                            Function newFunc = new Function();
                            newFunc.Name = this.getSub("func", funcStartParamPos).Trim(trim);
                            newFunc.Params = this.StringScheme.Substring(funcStartParamPos, funcStopParamPos - funcStartParamPos - 1).Split(',').ToList<String>();

                            string body = this.getSub(funcStopParamPos, "end").Trim(trim);
                            newFunc.Body = body.Substring(0, body.Length - 2);// parser bug

                            this.SDK.Functions.Add(newFunc);
                            deletePos("func");
                            deletePos("end");
                            deletePos("return");
                        }
                    }

                    if (Namespace != "")
                    {
                        if (this.SetPosIf("\n", i) && this.hasPos("return") && i > this.getPos("return"))
                        {
                            this.SDK._return = lang.parseLine(getSub("return", "\n"));
                        }
                    }

                }
                
                

           }
           catch
            {
                
            }//*/

            return this.SDK;
        }

        /*
         *  Добавляет или удалет свойство в PosData
         *  Вохвращяет true если сущесвует свойство
        */
        private bool ToggleProp(string Prop)
        {
            Prop = "is" + Prop;

            bool Result = this.PosData.ContainsKey(Prop);
            if (Result)
            {
                this.PosData.Remove(Prop);
            }
            else
            {
                this.PosData.Add(Prop, 1);
            }

            return !Result;
        }
        /*
         Проверяет является ли Sub подстрокой Схемы начиная с StartPos
         */
        private bool SubIs(string Sub, int StartPos/*, bool Single = false*/)
        {
            int SubLength = Sub.Length;
            return StartPos + SubLength <= this.StringScheme.Length && this.StringScheme.Substring(StartPos, SubLength) == Sub;
        }

        /*
         * Проверяет является ли Sub подстрокой Схемы начиная с StartPos
         * и записывает позицию
         * @return Результат
         */
        private bool SetPosIf(string Sub, int StartPos/*, bool Single = false*/)
        {

            int SubLength = Sub.Length;
            bool Result = StartPos + SubLength <= this.StringScheme.Length && this.StringScheme.Substring(StartPos, SubLength).ToLower() == Sub;
            if (Result)
            {
                this.PosData[Sub] = StartPos + SubLength;

            }
            return Result;
        }

        /*
         *  Вохвращяет подстроку между 2 другими, на основе сохранёных позиций
         */
        private string getSub(string Prop1, string Prop2)
        {
            if (this.PosData.ContainsKey(Prop1) && this.PosData.ContainsKey(Prop2))
            {
                int StartPos = this.PosData[Prop1];
                int SubLength = this.PosData[Prop2] - StartPos - 1;
                return this.StringScheme.Substring(StartPos, SubLength);
            }

            return "";
        }
        private string getSub(int StartPos, int StopPos) { return this.StringScheme.Substring(StartPos, StopPos - StartPos - 1); }
        private string getSub(string Prop1, int StopPos)
        {
            if (this.PosData.ContainsKey(Prop1))
            {
                int StartPos = this.PosData[Prop1];
                return this.getSub(StartPos, StopPos);
            }
            return "";
        }
        private string getSub(int StartPos, string Prop2)
        {
            if (this.PosData.ContainsKey(Prop2))
            {
                int StopPos = this.PosData[Prop2];
                return this.getSub(StartPos, StopPos);
            }
            return "";
        }

        private bool hasPos(string Prop1)
        {
            return this.PosData.ContainsKey(Prop1);
        }

        private int getPos(string Prop1)
        {
            return this.PosData[Prop1];
        }

        private bool deletePos(string Prop)
        {
            return this.PosData.Remove(Prop);
        }


    }
}
